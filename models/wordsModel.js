const mongoose = require('mongoose');

const wordSchema = mongoose.Schema({
  word: {
    type: String,
    lowercase: true,
    unique: true
  }
});

module.exports = mongoose.model('word', wordSchema)
