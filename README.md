1.Change the port and the DB Link in the .env file

2.To populate the data base run the process/updateWordsToDB

3.To add new words to DB add it to the nonLexicalWords array in defines/complexityDef.js then run  step 2

4.run server.js

	send text example : http://localhost:5000/complexity?text=Kim loves going to the cinema

	send array example : http://localhost:5000/complexity?mode=verbose&text=["Kim loves going to the cinema","Kim watched a good movie","Kim is awesome"]
