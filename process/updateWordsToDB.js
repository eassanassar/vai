require('dotenv').config({path: '../.env'});
const mongoose = require('mongoose');
const Word = require('../models/wordsModel');

//contains all the words to be inserted to the database, if you need to add anything new add it to this array
const {nonLexicalWords} = require('../defines/complexityDef');

// run the procces to insert/update all non Lexical Words to the Word Scheme
const run = ()=>{
  return new Promise(function(resolve, reject) {
    let bulkNLW = [];

    // create an array of the words to be inserted/updated from the nonLexicalWords
    nonLexicalWords.forEach(elem => {
      bulkNLW.push(
        { updateOne :
          {
             "filter" : {'word': elem},
             "update" : {'word': elem},
             "upsert" : true,
          }
       });
    });

    // save the words to DB in bulk
    Word.bulkWrite(bulkNLW).then(()=>{
      console.log("nonLexicalWords is Updated Successfuly");
      resolve();
    }).catch((err)=>{
      reject(err);
    });
  });
}

//connect to the database then close when finished with process
mongoose.connect(process.env.DB_LINK, { useNewUrlParser: true })
    .then(() => {
        console.info('Connect to DB succeed');
        console.info('Let\'s start fetch this data now');
        return run();
    })
    .catch((error) => {
        console.error(`updateWordsToDB Process, error is: ${error.message}`);
    })
    .then(() => {
        console.info('My job here is Done');
        mongoose.connection.close();
    });
