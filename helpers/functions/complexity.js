// gets array of objects [{word: 'STR1'},{word: 'STR2'} ...] and return a string 'STR1 STR2 ...'
const  getnonLexicalWordsString= (nonLexicalWordsArr)=>{
  let nonLexicalWordsStr = ''
  nonLexicalWordsArr.forEach(elem => {
    nonLexicalWordsStr+= `${elem.word} `;
  });
  return nonLexicalWordsStr;
}

/** return the lexicalDensity
* gets the text and array of objects [{word: 'STR1'},{word: 'STR2' }, ...] and retruns
* object {
*   n,  // number of words
*   nLex, // non lexecal words in the text
*   ld // the lexical Density
* }
**/
const lexicalDensity = (text, nonLexicalWordsArr)=>{
  const str = text.toLowerCase();
  const textArr = str.split(' ');
  let obj = {}

  // number of words
  obj.n = textArr.length;

  // changes the array of objects to string
  let nonLexicalWordsStr = getnonLexicalWordsString(nonLexicalWordsArr);

  // number of non Lexical Words
  obj.nLex = textArr.reduce((memo, value)=>{
    memo = nonLexicalWordsStr.includes(value)? memo+1: memo;
    return memo;
  },0);

  //lexical Density
  obj.ld = Math.floor((obj.nLex/obj.n)*100)/100;

  return obj;
}

/**gets the array of Strings and an Array of objects of the nonLexicalWords [{word: 'STR1'},{word: 'STR2' }, ...]
* returns an object similer to this: {
* sentence_ld: [ 0.23, 0.1, 1.0, 0.0],
* overall_ld: 0.42
* }
**/
const lexicalDensityArray = (textArr, nonLexicalWordsArr)=>{
  let obj;
  let sentence_ld = [];
  let n = 0
  let nLex = 0;

  textArr.forEach(elem => {
    obj = lexicalDensity(elem,nonLexicalWordsArr);
    sentence_ld.push(obj.ld);
    n+=obj.n;
    nLex += obj.nLex;
  });
  const overall_ld = Math.floor((nLex/n)*100)/100;
  
  return {sentence_ld, overall_ld};
};


const lexicalDensityText = (text, nonLexicalWordsArr)=>{
  const obj = lexicalDensity(text,nonLexicalWordsArr);
  const overall_ld = obj.ld;
  return {overall_ld};
};


module.exports = {
  lexicalDensityArray,
  lexicalDensityText
}
