const { validationResult } = require('express-validator/check');

const isValid = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.warn(`Validation Error in route: ${req.originalUrl}\nerrors: ${JSON.stringify(errors.mapped())}`);
        return res.status(422).json({error: errors.mapped()});
    } else {
        next();
    }
}

module.exports = {
  isValid
}
