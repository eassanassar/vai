const { query} = require('express-validator/check');
const {checkText} = require('./custom/complexityValCustom');

 const complexityValidate = [
   query('text')
   .exists().withMessage('text does not exist')
   .custom(checkText)
 ]

module.exports = {
  complexityValidate
}
