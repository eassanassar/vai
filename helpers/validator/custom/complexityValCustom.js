const checkText = (value, { req }) => {
  req.values = req.values === undefined? {} : req.values;
  const maxLength= 100;
  const maxWords= 10;
  // check array
  if(req.query.mode === 'verbose'){
      const arr = JSON.parse(req.query.text);
      if(!Array.isArray(arr)){
        throw new Error('text should be an Array when mode=verbose');
      }
      arr.forEach(elem => {
          //Only texts with up to ${maxWords} words or up to ${maxLength} characters
          if(!(elem.length < maxLength && elem.split(' ').length < maxWords)){
              throw new Error(`texts must be up to ${maxWords} words or up to ${maxLength} characters `);
          }
      });
      req.values['text']= arr;
  }else{
    if(value.length < maxLength && value.split(' ').length < maxWords){
      req.values['text']= req.query.text;
    }else{
      throw new Error(`texts must be up to ${maxWords} words or up to ${maxLength} characters `);
    }
  }
  return true
}


module.exports = {
  checkText
}
