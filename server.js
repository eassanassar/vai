require('dotenv').config();
const express = require('express')
const app = express()
const mongoose = require('mongoose');

const port = process.env.PORT || 5000 ;
const complexityRoute = require('./api/complexity');

console.log(process.env.DB_LINK);
//mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_LINK, { useNewUrlParser: true });
//test connection
mongoose.connection.once('open', function(){
    console.log('mongoDB connected sucessfully !!');
}).on('error', function(error){
    console.log('MongoDB Connection error: ', error);
});


app.use('/complexity', complexityRoute);

//404 if couldnt find route
app.use((req, res, next) =>{
    const error = new Error('Route not found');
    error.status = 404;
    next(error)
});

// Error handling
app.use((error, req, res, next)=>{
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
})


app.listen(port, () => console.log(`app listening on port ${port}`));
