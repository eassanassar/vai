const express = require('express');
const router = express.Router();
const complexityCtrl = require('../controllers/complexityCtrl');
const validator = require('../helpers/validator/validator')
const val = require('../helpers/validator/complexityVal')

router.get('/',val.complexityValidate, validator.isValid, complexityCtrl.lexecalWords);

module.exports = router;
