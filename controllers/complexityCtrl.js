const functions = require('../helpers/functions/complexity');
const mongoose = require('mongoose');
const Word = require('../models/wordsModel');

const lexecalWords = ( async (req, res) => {
  try{
    //TODO: get text from req.query.text
  //  const text = `Kim loves going to the cinema`;
  //  const arr = [text, text, text];
    const text = req.values.text;
    const nonLexicalWords = await Word.find({},{'_id':0});
    let data ={};
    if(req.query.mode ==='verbose'){
      data = functions.lexicalDensityArray(text, nonLexicalWords);
    }else{
      data = functions.lexicalDensityText(text, nonLexicalWords);
    }

    res.json({data});

  }catch(err){
    res.status(500).json({
      error: `complexityCtrl - lexecalWords, err: ${err}`
    });
  }

});



module.exports = {
  lexecalWords
}
